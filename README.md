# What is Spring Boot RestQL?
In short terms, restql can filter big objects (enitites comming from db) to reduce bandwith, frontend computation and strip unneeded or critical data
before sending it to the frontend

# How to install?

    <repositories>
        <repository>
            <id>gitlab-maven</id>
            <url>https://gitlab.com/api/v4/projects/27026028/packages/maven</url>
        </repository>
    </repositories>

    <dependencies>
        <dependency>
            <groupId>de.sauerbier</groupId>
            <artifactId>spring-boot-restql</artifactId>
            <version>1.0-SNAPSHOT</version>
        </dependency>

    </dependencies>

# How to use?
## Old way
```java
  //Autowire the factory service into your controller / service
  @Autowired private RestQLFactory restQLFactory;

  @PostMapping("/api/user")
  public RestQLResponse getUser(@RequestParam("user") String user, @RequestParam(required = false) String[] fields){
      //retrieve the user from our imaginary UserRepo
      User user = this.userRepository.getUser(user);
      //now we only want to return the requested data in the fields parameter, sent over by the frontend.
      return this.restQLFactory.filter(user, fields);
  }
```
## New way
```java

  @PostMapping("/api/user")
  @RestQL
  public RestQLResponse getUser(@RequestParam("user") String user, @RequestParam(required = false) String[] fields){
      //retrieve the user from our imaginary UserRepo and return it
      // this method will ne wrapped by RestQL on execution and automatically filters the returned object
      //authorization checks still apply.
      return this.userRepository.getUser(user);
  }
```

The mentioned fields above can be one ore multiple RestQL's queries which specify Object fields that should be returned<br>
Lets imagine our User looks like this:<br>

```java
public class User{

    private Long id;
    private Boolean enabled;
    private String password;
    private String username;
    private String firstName;
    private String lastName;
    private String email;
    private String jobTitle;
    private String phone;
    private String location;

    private List<Groups> groups;
    private List<Authority> authorities;
}
```

On the root Object we can specify fields as the following:<br>
`?fields=username,firstName,lastName,email`<br>
Now only these fields will be returned in json looking like this:<br>
```json
 "data": {
        "firstName": "Max",
        "lastName": "Musterman",
        "email": "test@example.com",
        "username": "admin"
}
```

We can also iterate collections using the `$` operator or a number as index:<br>
`?fields=username,firstName,lastName,email,groups.$.name`<br>
To only select a specific index one can use this syntax (i.e. index 1):<br>
`?fields=username,firstName,lastName,email,groups.1.name`<br>
Which will return:<br>
```json
"data": {
        "firstName": "Max",
        "lastName": "Musterman",
        "groups": [
            {
                "name": "ADMIN"
            },
            {
                "name": "SUPPORT"
            }
        ],
        "email": "test@example.com",
        "username": "admin"
    }
```

Multiple fields on the same object level can be specifed using the `:` operator:<br>
`?fields=username,firstName,lastName,email,groups.$.name:description`<br>
Which will return:<br>
```json
"data": {
        "firstName": "Max",
        "lastName": "Musterman",
        "groups": [
            {
                "name": "ADMIN",
                "description": "Administrator of this system"
            },
            {
                "name": "SUPPORT",
                "description": "IT support for clients"
            }
        ],
        "email": "test@example.com",
        "username": "admin"
    }
```

# RestQLSerializer
serializers can be used to manipulate the filtering process of each object.<br>
They are usefull to restrict serialisation or to customize how a fields gets serialized.<br>
Custom serializers can be registered like this:<br>

```java
@Component
class ComputeMethodSerializer implements RestQLSerializer {

    @Override
    public String[] buildTargetFieldSet(RestQLSerializationContext context, Object target, String... fields) {
       return fields;
    }

    @Override
    public boolean authorizeSerialisation(RestQLSerializationContext context, Member member, String path, Object target, boolean currentState){
        if(member instanceof Method){
           return ((Method) member).isAnnotationPresent(Compute.class);
        }else return true;
    }

    @Override
    public Object serialize(RestQLSerializationContext context, Member member, String path, Object target){
      return target;
    }
}
```

This implementation above is a good example of how we can use serializers.<br>
It will allow serialization of methods thats are annotated by `@Computed`<br>
This serializer is provided out of the box and can be used after installing RestQL.<br>
The handy thing about `@Computed` is, that we can annotate methods inside classes that act as a field uppon serialization<br>
Example:<br>
We extend our User class from above<br>

```java
public class User{
     private Long id;
    private Boolean enabled;
    private String password;
    private String username;
    private String firstName;
    private String lastName;
    private String email;
    private String jobTitle;
    private String phone;
    private String location;

    @Computed
    public String fullname(){
        return this.firstName + " " + this.lastName;
    }
}
```

now we can use the `fullname` method as a field in our qeury like this:<br>
`?fields=username,fullname,email,groups.$.name:description`<br>
And the method will be executed while serialising the object:<br>
```json
"data": {
        "fullname": "Max Mustermann",
        "groups": [
            {
                "name": "ADMIN",
                "description": "Administrator of this system"
            },
            {
                "name": "SUPPORT",
                "description": "IT support for clients"
            }
        ],
        "email": "test@example.com",
        "username": "admin"
    }
```
# AuthenticatedSerializer
This Serializer is also provided out of the box and allows you to annotate fields and methods with `@Auhtority("permission")` 
which will automatically filter out any data that the supplied entity has no authority to access to.<br>
Example:<br>
Lets add the `@authority` annotation to the password and email field of the user:<br>
```java
public class User{

    private Long id;
    private Boolean enabled;
    @Authority("permissions.user.password.read")
    private String password;
    private String username;
    private String firstName;
    private String lastName;
    @Authority("permissions.user.email.read")
    private String email;
    private String jobTitle;
    private String phone;
    private String location;

    private List<Groups> groups;
    private List<Authority> authorities;
}
```

Now lets refactor our controller like this:
```java
  //Autowire the factory service into your controller / service
  @Autowired private RestQLFactory restQLFactory;

  @PostMapping("/api/user")
  public RestQLResponse getUser(Principal principal, @RequestParam("user") String user, @RequestParam(required = false) String[] fields){
      //Load the user making this request
      User currentUser = this.userRepository.getUser(principal.getName());
      //Load the user which the current user is trying to access
      User user = this.userRepository.getUser(user);
      //now we supply the filter methods this the current users authorities and RestQL will filter out any fields not matching
      //the granted list of permnissions the current user has
      return this.restQLFactory.filter(user.getAuthorities(), user, fields);
```

The `Authority` class implemented the interface `Permission`<br>
Which just supplies the method `String getAuthority()` in order to compare a users granted perimissions and the permission needed to acces a field.<br>
If we would now request those fields with a user that has no permission to access them they will just be ignored:<br>
`?fields=username,fullname,email,password`<br>
We will get:
```json
 "data": {
        "fullname": "Max Mustermann",
        "username": "admin"
}
```
