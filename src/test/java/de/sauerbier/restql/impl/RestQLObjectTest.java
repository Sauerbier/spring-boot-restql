package de.sauerbier.restql.impl;

class RestQLObjectTest {

    //TODO: migrate

    /*private static final List<RestQLSerializer> SERIALIZERS = new ArrayList<>() {{
        add(new ComputeMethodSerializer());
        add(new AuthenticatedSerializer());
    }};

    private static final User TEST_USER = Mocks.createFullUser();
    private static final List<User> TEST_USER_LIST = Arrays.asList(TEST_USER, Mocks.createFullUser());

    @BeforeEach
    void setUp() {
    }

    @Test
    void testNoFilter() {
        VueResponseFilteredObject filtered = new VueResponseFilteredObject(20000, TEST_USER);
        assertEquals(TEST_USER, filtered.getData());
    }

    @Test
    void testNoFilterList() {
        VueResponseFilteredObject filtered = new VueResponseFilteredObject(20000, TEST_USER_LIST);
        assertDoesNotThrow(() -> (filtered.getData().getClass().cast(List.class.getComponentType())));
        assertEquals(TEST_USER, ((List) filtered.getData()).get(0));
    }


    @Test
    void testWithFilter() {
        VueResponseFilteredObject filtered = new VueResponseFilteredObject(20000, TEST_USER, "username", "firstName", "groups.$.name:id");
        HashMap<String, Object> data = (HashMap<String, Object>) filtered.getData();
        assertAll(() -> {
            assertNotNull(data);
            assertEquals(3, data.size());
            assertNotNull(data.get("groups"));
            assertNotNull(((ArrayList) data.get("groups")).get(0));
            assertEquals(2, ((HashMap) ((ArrayList) data.get("groups")).get(0)).size());
        });
    }

    @Test
    void testWithFilterList() {
        VueResponseFilteredObject filtered = new VueResponseFilteredObject(20000,TEST_USER_LIST, "username", "firstName", "groups.$.name:id");
        List<Object> data = (List<Object>) filtered.getData();
        assertAll(() -> {
            assertNotNull(data);
            assertNotNull(data.get(0));
            assertEquals(3, ((HashMap) data.get(0)).size());
            assertNotNull(((HashMap<?, ?>) data.get(0)).get("firstName"));
            assertEquals("admin",((HashMap) ((List) ((HashMap<?, ?>) data.get(0)).get("groups")).get(0)).get("name"));
        });
    }

    @Test
    void testWithFilterListWithSelector() {
        VueResponseFilteredObject filtered = new VueResponseFilteredObject(20000, TEST_USER_LIST, "username", "firstName", "groups.0.name:id");
        List<Object> data = (List<Object>) filtered.getData();
        assertAll(() -> {
            assertNotNull(data);
            assertNotNull(data.get(0));
            assertEquals(3, ((HashMap) data.get(0)).size());
            assertNotNull(((HashMap<?, ?>) data.get(0)).get("firstName"));
            assertEquals("admin",((HashMap) ((HashMap<?, ?>) data.get(0)).get("groups")).get("name"));
        });
    }


    @Test
    void testNoFilterWithSerializer() {
        VueResponseFilteredObject filtered = new VueResponseFilteredObject(20000, TEST_USER,SERIALIZERS);
        assertEquals(TEST_USER, filtered.getData());
    }

    @Test
    void testNoFilterListWithSerializer() {
        VueResponseFilteredObject filtered = new VueResponseFilteredObject(20000, TEST_USER_LIST,SERIALIZERS);
        assertDoesNotThrow(() -> (filtered.getData().getClass().cast(List.class.getComponentType())));
        assertEquals(TEST_USER, ((List) filtered.getData()).get(0));
    }


    @Test
    void testWithFilterWithSerializer() {
        VueResponseFilteredObject filtered = new VueResponseFilteredObject(20000, TEST_USER,SERIALIZERS, "username", "firstName", "groups.$.name:id");
        HashMap<String, Object> data = (HashMap<String, Object>) filtered.getData();
        assertAll(() -> {
            assertNotNull(data);
            assertEquals(3, data.size());
            assertNotNull(data.get("groups"));
            assertNotNull(((ArrayList) data.get("groups")).get(0));
            assertEquals(2, ((HashMap) ((ArrayList) data.get("groups")).get(0)).size());
        });
    }

    @Test
    void testWithFilterWithSelectorWithSerializer() {
        VueResponseFilteredObject filtered = new VueResponseFilteredObject(20000, TEST_USER,SERIALIZERS, "username", "firstName", "groups.0.name:id");
        HashMap<String, Object> data = (HashMap<String, Object>) filtered.getData();
        assertAll(() -> {
            assertNotNull(data);
            assertEquals(3, data.size());
            assertNotNull(data.get("groups"));
            assertEquals("admin", ((HashMap) data.get("groups")).get("name"));
        });
    }

    @Test
    void testWithFilterListWithSerializer() {
        VueResponseFilteredObject filtered = new VueResponseFilteredObject(20000, TEST_USER_LIST,SERIALIZERS,  "username", "firstName", "groups.$.name:id");
        List<Object> data = (List<Object>) filtered.getData();
        assertAll(() -> {
            assertNotNull(data);
            assertNotNull(data.get(0));
            assertEquals(3, ((HashMap) data.get(0)).size());
            assertNotNull(((HashMap<?, ?>) data.get(0)).get("firstName"));
            assertEquals("admin",((HashMap) ((List) ((HashMap<?, ?>) data.get(0)).get("groups")).get(0)).get("name"));
        });
    }*/
}