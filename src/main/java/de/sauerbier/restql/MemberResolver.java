package de.sauerbier.restql;

import java.lang.reflect.Member;

public abstract class MemberResolver implements RestQLSerializer{

    @Override
    public String[] buildTargetFieldSet(final RestQLSerializationContext context, final Object parent, final Object target, final String... fields) {
        return fields;
    }

    @Override
    public boolean authorizeSerialisation(final RestQLSerializationContext context, final Member member, final String path, final Object target, final boolean currentAuthState) {
        return currentAuthState;
    }

    @Override
    public Object serialize(final RestQLSerializationContext context, final Member member, final String path, final Object parent, final Object target) {
        return target;
    }
}
