package de.sauerbier.restql;

public interface RestQLResponse<T> {

    T getData();
    void setData(T data);
}
