package de.sauerbier.restql;

public interface RestQLSerializationContext {
    RestQLSerializationContext DEFAULT_CONTEXT = new DefaultSerializationContext();


    class DefaultSerializationContext implements RestQLSerializationContext{

    }

}
