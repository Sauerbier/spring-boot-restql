package de.sauerbier.restql;

import org.springframework.security.core.GrantedAuthority;

public interface Permission extends GrantedAuthority {

}
