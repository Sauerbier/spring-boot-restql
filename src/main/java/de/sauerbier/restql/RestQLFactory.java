package de.sauerbier.restql;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

/**
 * @Author Sauerbier | Jan
 * @Copyright 2020 by Jan Hof
 * All rights reserved.
 **/
@Service
public interface RestQLFactory{

    /**
     * Filters target object fields and method and returns an anonymous object built with map
     * for further serialization (to json etc...)
     *
     * @param object to be filtered
     * @param fields object fields that should be returned
     * @return filtered object or whole object if fields are null
     */
    RestQLResponse<?> filter(Object object, String[] fields);

    /**
     * Filters target object fields and method and returns an anonymous object built with maps
     * for further serialization (to json etc...)
     * Strips any fields the user has no permission to fetch
     * This method is useful for object's children that need further permission to be accessed
     *
     * @param permissions the user currently has
     * @param object to be filtered
     * @param fields object fields that should be returned
     * @return filtered object or null if fields are null
     */
    RestQLResponse<?> filter(Collection<? extends GrantedAuthority> permissions, Object object, String[] fields);

}
