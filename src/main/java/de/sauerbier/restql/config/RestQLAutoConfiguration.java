package de.sauerbier.restql.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("de.sauerbier.restql")
public class RestQLAutoConfiguration {
}
