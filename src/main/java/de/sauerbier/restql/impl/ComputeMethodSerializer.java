package de.sauerbier.restql.impl;

import java.lang.reflect.Member;
import java.lang.reflect.Method;

import org.springframework.stereotype.Component;

import de.sauerbier.restql.Compute;
import de.sauerbier.restql.MapFieldsToGetters;
import de.sauerbier.restql.RestQLSerializationContext;
import de.sauerbier.restql.RestQLSerializer;

/**
 * @Author Sauerbier | Jan
 * @Copyright 2020 by Jan Hof
 * All rights reserved.
 **/

@Component
class ComputeMethodSerializer implements RestQLSerializer {

    @Override
    public String[] buildTargetFieldSet(RestQLSerializationContext context, Object parent, Object target, String... fields) {
        return fields;
    }

    @Override
    public boolean authorizeSerialisation(RestQLSerializationContext context, Member member, String path, Object target, boolean currentState) {
        if (member instanceof Method) {
            return ((Method) member).isAnnotationPresent(Compute.class) || ((Method) member)
                                                                             .getDeclaringClass()
                                                                             .isAnnotationPresent(MapFieldsToGetters.class) || ((Method) member)
                                                                                                                                 .getDeclaringClass()
                                                                                                                                 .getSuperclass()
                                                                                                                                 .isAnnotationPresent(
                                                                                                                                   MapFieldsToGetters.class);
        } else {
            return true;
        }
    }

    @Override
    public Object serialize(RestQLSerializationContext context, Member member, String path, Object parent, Object target) {
        return target;
    }

    @Override
    public Member memberResolver(final RestQLSerializationContext context, final String memberName, final String path, final Member possibleMember, final Class<?> clazz) {
        return possibleMember;
    }

}
