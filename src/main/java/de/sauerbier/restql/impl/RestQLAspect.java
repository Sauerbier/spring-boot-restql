package de.sauerbier.restql.impl;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import de.sauerbier.restql.RestQL;
import de.sauerbier.restql.RestQLFactory;
import de.sauerbier.restql.RestQLResponse;

@Aspect
@Order(Ordered.HIGHEST_PRECEDENCE)
@Component
public class RestQLAspect {

    private final RestQLFactory restQLFactory;

    @Autowired
    public RestQLAspect(final RestQLFactory restQLFactory) {this.restQLFactory = restQLFactory;}

    @Around("@annotation(de.sauerbier.restql.RestQL)")
    public Object filter(final ProceedingJoinPoint pjp) throws Throwable {
        final Method method = ((MethodSignature) pjp.getSignature()).getMethod();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (method != null) {
            final RestQL rest = method.getAnnotation(RestQL.class);
            final HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();

            String[] filters = null;

            if (request.getParameterValues(rest.value()) != null) {
                filters = Arrays
                  .stream(request.getParameterValues(rest.value()))
                  .filter(Objects::nonNull)
                  .flatMap(f -> Arrays.stream(f.split(",")))
                  .toArray(String[]::new);
            }

            final Object result = pjp.proceed();
            RestQLResponse<?> filtered;

            if (auth != null && auth.getAuthorities() != null) {
                filtered = this.restQLFactory.filter(auth.getAuthorities(), result, filters);
            } else {
                filtered = this.restQLFactory.filter(result, filters);
            }

            return rest.raw() ? filtered.getData() : filtered;
        }

        return pjp.proceed();
    }

}
