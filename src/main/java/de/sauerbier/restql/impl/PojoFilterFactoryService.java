package de.sauerbier.restql.impl;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import de.sauerbier.restql.RestQLFactory;
import de.sauerbier.restql.RestQLResponse;
import de.sauerbier.restql.RestQLSerializer;

/**
 * @Author Sauerbier | Jan
 * @Copyright 2020 by Jan Hof
 * All rights reserved.
 **/
@Service
class PojoFilterFactoryService implements RestQLFactory {

    private final List<RestQLSerializer> serializers;

    @Autowired
    public PojoFilterFactoryService(List<RestQLSerializer> serializers) {
        this.serializers = serializers;
    }

    @Override
    public RestQLResponse<Object> filter(Object object, String[] fields){
        return new RestQLObject(object, serializers, fields);
    }

    @Override
    public RestQLResponse<Object> filter(Collection<? extends GrantedAuthority> permissions, Object object, String[] fields){
        return new RestQLObject(object, serializers ,new PermissionContext(permissions), fields);
    }
}
