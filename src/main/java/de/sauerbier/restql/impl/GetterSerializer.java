package de.sauerbier.restql.impl;

import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Method;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import de.sauerbier.restql.MapFieldsToGetters;
import de.sauerbier.restql.MemberResolver;
import de.sauerbier.restql.RestQLSerializationContext;

@Component
public class GetterSerializer extends MemberResolver {

    @Override
    public Member memberResolver(final RestQLSerializationContext context, final String memberName, final String path, final Member possibleMember, final Class<?> clazz) {
        if (clazz.isAnnotationPresent(MapFieldsToGetters.class)) {
            if (possibleMember instanceof Field) {
                final Field field = (Field) possibleMember;

                String prefix = field.getType().isAssignableFrom(Boolean.TYPE) ? "is" : "get";

                try {
                    return field.getDeclaringClass().getMethod(prefix + StringUtils.capitalize(field.getName()));
                } catch (NoSuchMethodException e) {
                    try {
                        return field.getDeclaringClass().getMethod(field.getName());
                    } catch (NoSuchMethodException ex) {
                        return possibleMember;
                    }
                }
            } else {
                try {
                    return clazz.getMethod("get" + StringUtils.capitalize(memberName));
                } catch (NoSuchMethodException e) {
                    final Method method;
                    try {
                        method = clazz.getMethod("is" + StringUtils.capitalize(memberName));
                    } catch (NoSuchMethodException ex) {
                        try {
                            return clazz.getMethod(memberName);
                        } catch (NoSuchMethodException exc) {
                            return possibleMember;
                        }
                    }
                    return method;
                }
            }
        }
        return possibleMember;
    }

}