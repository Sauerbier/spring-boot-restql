package de.sauerbier.restql.impl;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;

import de.sauerbier.restql.RestQLSerializationContext;

public class PermissionContext implements RestQLSerializationContext {


    private Collection<? extends GrantedAuthority> permissions;

    public PermissionContext(Collection<? extends GrantedAuthority> permissions) {
        this.permissions = permissions;
    }

    public Collection<? extends GrantedAuthority> getPermissions(){
        return permissions;
    }

    public void setPermissions(Collection<? extends GrantedAuthority> permissions){
        this.permissions = permissions;
    }
}
