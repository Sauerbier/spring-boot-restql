package de.sauerbier.restql.impl;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.collect.Iterables;

import de.sauerbier.restql.RestQLResponse;
import de.sauerbier.restql.RestQLSerializationContext;
import de.sauerbier.restql.RestQLSerializer;

/**
 * @Author Sauerbier | Jan
 * @Copyright 2019 by Jan Hof
 * All rights reserved.
 **/

/**
 * Manually instantiate this class if all fields & methods are allowed to be fetched by the client
 * Otherwise please use the @VueResponseFilterFactory that will strip any data the client is not allowed to fetch
 *
 * Even though this class seems to be heavy its computation time is irrelevant (5ms for 2kb data) an average request takes about 400ms
 */
class RestQLObject implements RestQLResponse<Object> {

    private static final Pattern INDEX_PATTERN = Pattern.compile(".*#([0-9].+).*");

    private Object data;

    @JsonIgnore
    private transient List<RestQLSerializer> serializers = Collections.emptyList();
    @JsonIgnore
    private transient final RestQLSerializationContext context;

    public RestQLObject(Object target, String... fields) {
        init(target, fields);
        this.context = RestQLSerializationContext.DEFAULT_CONTEXT;
    }

    public RestQLObject(Object target, List<RestQLSerializer> serializers, RestQLSerializationContext context, String... fields) {
        this.serializers = serializers;
        this.context = context;
        init(target, fields);
    }

    public RestQLObject(Object target, List<RestQLSerializer> serializers, String... fields) {
        this(target, serializers, RestQLSerializationContext.DEFAULT_CONTEXT, fields);
    }

    protected void init(Object target, String... fields) {

        for (RestQLSerializer serializer : serializers) {
            fields = serializer.buildTargetFieldSet(context, target, target, fields);
        }

        if (fields != null && fields.length > 0) {
            List<HashMap<String, Object>> maps = new ArrayList<>();
            if (target.getClass().isArray()) {
                for (final Object t : ((Object[]) target)) {
                    maps.add(accessRootLevelObject(t, fields));
                }
                this.data = maps;
            } else if (target instanceof Collection) {
                for (Object t : (Collection<?>) target) {
                    maps.add(accessRootLevelObject(t, fields));
                }
                this.data = maps;
            } else {
                this.data = accessRootLevelObject(target, fields);
            }
        } else {
            this.data = target;
        }
    }

    private Object trace(Object target, String path) {
        if (target == null) {
            return null;
        }
        if (path == null || path.isEmpty()) {
            return target;
        }

        Map<String, Object> root = new HashMap<>();

        String currentPath = nextChild(path);
        if (!path.equals(currentPath)) {
            path = path.substring(currentPath.length() + 1);
        } else {
            path = "";
        }

        boolean end = path.startsWith(";");

        if (currentPath.equals("$")) {
            List<Object> list = new ArrayList<>();
            for (Object t : (Collection<?>) target) {
                Object trace = trace(t, path);
                if (trace != null) {
                    list.add(trace);
                }
            }

            return list;
        } else if (StringUtils.isNumeric(currentPath)) {
            int index = Integer.parseInt(currentPath);
            Collection targetList = (Collection) target;
            if (index < targetList.size()) {
                Object trace = Iterables.get(targetList, index);
                return trace(trace, path);
            } else {
                return null;
            }
        } else if (currentPath.contains(":")) {
            for (String split : getFields(currentPath)) {
                String field = split.split("\\.")[0];
                Member fieldOrMethod = getFieldOrMethod(currentPath, field, target);
                if (fieldOrMethod == null) {
                    continue;
                }
                Object child = getOrInvoke(fieldOrMethod, target);
                if (child == null) {
                    continue;
                }
                String childPath = field.equals(split) ? field : split.substring(field.length() + 1);
                if (field.equals(split)) {
                    root.put(field, child);
                    continue;
                }
                Object o = computeSerializers(fieldOrMethod, childPath, target, end ? child : trace(child, childPath));
                root.put(field, o);
            }
            Object trace = trace(target, path);
            if (trace instanceof Map) {
                root.putAll((Map<? extends String, ?>) trace);
            }
            return root;
        } else {
            Member fieldOrMethod = getFieldOrMethod(currentPath, currentPath, target);
            if (fieldOrMethod == null) {
                root.put(currentPath, null);
                return root;
            }

            Object child = getOrInvoke(fieldOrMethod, target);
            if (child == null) {
                return root;
            }

            Object o = computeSerializers(fieldOrMethod, currentPath, target, end ? child : trace(child, path));
            root.put(currentPath, o);
            return root;
        }
    }

    private HashMap<String, Object> accessRootLevelObject(Object t, String[] fields) {
        HashMap<String, Object> map = new HashMap<>();
        for (String field : fields) {
            if (field.contains(".")) {
                map.putAll((Map<? extends String, ?>) trace(t, field));
            } else {
                Member fieldOrMethod = getFieldOrMethod(field, field, t);
                Object o = computeSerializers(fieldOrMethod, field, t, getOrInvoke(fieldOrMethod, t));
                if (o == null) {
                    continue;
                }
                map.put(field, o);
            }
        }
        return map;
    }

    private Member getFieldOrMethod(String currentPath, String name, Object target) {
        Member member = null;
        Class<?> current = target.getClass();
        do {
            try {
                member = current.getDeclaredField(name);
                ((AccessibleObject) member).setAccessible(true);
                break;
            } catch (NoSuchFieldException e) {
                try {
                    member = current.getDeclaredMethod(name);
                    ((AccessibleObject) member).setAccessible(true);
                    break;
                } catch (NoSuchMethodException ex) {
                    member = resolveMember(name, currentPath, null, current);
                    if (member != null) {
                        break;
                    }
                    current = current.getSuperclass();
                }
            }
        } while (current.getSuperclass() != null);
        if (member != null && !authorizeSerialisation(member, currentPath, target)) {
            return null;
        }

        return resolveMember(name, currentPath, member, current);
    }

    private Object getOrInvoke(Member fieldOrMethod, Object target) {
        try {
            if (fieldOrMethod instanceof Field) {
                return ((Field) fieldOrMethod).get(target);
            } else if (fieldOrMethod instanceof Method) {
                return ((Method) fieldOrMethod).invoke(target);
            }
        } catch (IllegalAccessException | InvocationTargetException e) {
            return null;
        }
        return null;
    }

    private String nextChild(String path) {
        if (path.indexOf('.') < 0 && !path.contains(":") && !path.contains(";")) {
            return path;
        }
        if (path.startsWith(":")) {
            return path;
        }

        if (path.contains(";")) {
            path = path.substring(0, path.indexOf(";"));
        }

        StringBuilder child = new StringBuilder();
        boolean fieldMode = false;
        char[] charArray = path.toCharArray();
        for (int i = 0; i < charArray.length; i++) {
            char c = charArray[i];
            if (c == '.' && !fieldMode) {
                break;
            }
            if (c == ':') {
                fieldMode = true;
            }
            if (c == ';') {
                break;
            }
            child.append(c);
        }
        return child.toString();
    }

    private List<String> getFields(String path) {
        ArrayList<String> fields = new ArrayList<>();
        StringBuilder builder = new StringBuilder();
        boolean traverseMode = false;

        for (char c : path.toCharArray()) {
            if (c == '.') {
                traverseMode = true;
            }
            if (c == ':' && !traverseMode) {
                fields.add(builder.toString());
                builder = new StringBuilder();
                continue;
            }
            builder.append(c);
        }
        fields.add(builder.toString());
        return fields;
    }

    private Member resolveMember(String member, String path, Member possibleMember, Class<?> clazz) {
        Member targetMember = null;
        for (RestQLSerializer serializer : this.serializers) {
            targetMember = serializer.memberResolver(context, member, path, possibleMember, clazz);
        }
        return targetMember;
    }

    private Object computeSerializers(Member member, String path, Object parent, Object target) {
        for (RestQLSerializer serializer : this.serializers) {
            target = serializer.serialize(context, member, path, parent, target);
        }
        return target;
    }

    private boolean authorizeSerialisation(Member member, String path, Object target) {
        boolean authorized = true;
        for (RestQLSerializer serializer : this.serializers) {
            authorized = serializer.authorizeSerialisation(context, member, path, target, authorized);
        }
        return authorized;
    }

    @Override
    public Object getData() {
        return this.data;
    }

    @Override
    public void setData(Object data) {
        this.data = data;
    }

    public List<RestQLSerializer> getSerializers() {
        return serializers;
    }

    public void setSerializers(List<RestQLSerializer> serializers) {
        this.serializers = serializers;
    }

    public void addSerializer(int priority, RestQLSerializer serializer) {
        this.serializers.add(priority, serializer);
    }

}
