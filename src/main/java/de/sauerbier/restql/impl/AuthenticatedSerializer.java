package de.sauerbier.restql.impl;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Member;
import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import de.sauerbier.restql.Authority;
import de.sauerbier.restql.RestQLSerializationContext;
import de.sauerbier.restql.RestQLSerializer;

/**
 * @Author Sauerbier | Jan
 * @Copyright 2020 by Jan Hof
 * All rights reserved.
 **/
@Component
class AuthenticatedSerializer implements RestQLSerializer {

    public AuthenticatedSerializer() {}

    @Override
    public String[] buildTargetFieldSet(RestQLSerializationContext context,Object parent, Object target, String... fields) {
        return fields;
    }

    @Override
    public boolean authorizeSerialisation(RestQLSerializationContext context, Member member, String path, Object target, boolean currentState) {
        if (!currentState) {
            return false;
        }
        if (context instanceof PermissionContext) {
            if (((AnnotatedElement) member).isAnnotationPresent(Authority.class)) {
                Authority auth = ((AnnotatedElement) member).getAnnotation(Authority.class);
                Collection<? extends GrantedAuthority> permissions = ((PermissionContext) context).getPermissions();
                if (permissions == null) {
                    return false;
                }
                return permissions.stream().map(GrantedAuthority::getAuthority).anyMatch(a -> a.equals(auth.value()));
            }
        }
        return true;
    }

    @Override
    public Object serialize(RestQLSerializationContext context, Member member, String path, Object parent, Object target) {
        return target;
    }

    @Override
    public Member memberResolver(final RestQLSerializationContext context, final String memberName, final String path, final Member possibleMember, final Class<?> clazz) {
        return possibleMember;
    }

}
