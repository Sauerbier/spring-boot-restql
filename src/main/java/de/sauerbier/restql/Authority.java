package de.sauerbier.restql;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Author Sauerbier | Jan
 * @Copyright 2020 by Jan Hof
 * All rights reserved.
 **/
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.FIELD})
public @interface Authority{
    String value() default "";
}
