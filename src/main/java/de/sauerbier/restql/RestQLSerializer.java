package de.sauerbier.restql;

import java.lang.reflect.Member;

import org.springframework.stereotype.Component;

/**
 * @Author Sauerbier | Jan
 * @Copyright 2020 by Jan Hof
 * All rights reserved.
 **/
@Component
public interface RestQLSerializer {

    /**
     *
     * Manipulate the selected fields passed in by the client.
     * This method is executed before accessing the target object
     * @param context contains additional parameters needed for custom serialization
     * @param target the object that should be serialized
     * @param fields the originally selected fields
     * @return modified instance of fields or just fields
     */
    String[] buildTargetFieldSet(RestQLSerializationContext context,Object parent, Object target, String ... fields);

    /**
     * This method defines whether or not serializing the current member should be allowed
     * @param context contains additional parameters needed for custom serialization
     * @param member method or field of target object
     * @param path string representation of either the member's name or canonical path to children of target
     * @param target object to be serialized
     * @param currentAuthState whether or not serialization was authorized previously by a VueFilterSerializer
     * @return true if serialization is permitted
     */
    boolean authorizeSerialisation(RestQLSerializationContext context, Member member,String path,Object target, boolean currentAuthState);

    /**
     * This method can either serialize the target directly or modify the target / return that will then be serialized be system
     * @param context contains additional parameters needed for custom serialization
     * @param member current field/method representation
     * @param path string representation of either the member's name or canonical path to children of target
     * @param target object to be serialized
     * @return modified target or just target
     */
    Object serialize(RestQLSerializationContext context, Member member,String path,Object parent, Object target);

    Member memberResolver(RestQLSerializationContext context, String memberName, String path, Member possibleMember, Class<?> clazz);
}
